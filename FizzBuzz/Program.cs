using System;

namespace FizzBuzz
{
	/// <summary>
	/// FizzBuzz
	/// </summary>
	class MainClass
	{
		/// <summary>
		/// Print out the goods
		/// </summary>
		/// <param name="args">The command-line arguments.</param>
		public static void Main (string[] args)
		{
			for (int i = 1; i <= 100; i++) {
				string output = string.Empty;
				if (i % 3 == 0) output += "Fizz";
				if (i % 5 == 0) output += "Buzz";
				if (string.IsNullOrEmpty (output))
					output = i.ToString();
				Console.WriteLine (output);
			}
		}
	}
}
